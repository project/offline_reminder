
-- SUMMARY --

Offline Reminder sends a reminder email to an email address if the site has
been set to offline for longer than a configurable period.

For a full description of the module, visit the project page:
  http://drupal.org/project/offline_reminder

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/offline_reminder


-- REQUIREMENTS --

None


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions >>
  system module:
  
  - administer site configuration
  
    Users in roles with the "administer site configuration" permission will
    be able to change the settings for Offline Reminder
    
* Customize the module's settings in Administer >> Site configuration >>
  Offline Reminder.
  
  - Email Address:
    
    This setting is where the email will be sent to.
    
  - Time before email (in hours):
    
    This is the time period, in hours, that the module will wait
    before sending an email. For periods less than an hour, use decimals:
    0.5 for 30 minutes and 0.75 for 45 minutes. An email will be sent every
    time period until the site is back online. For example, if the period is
    1 hour, and the site is offline for 6 hours, the admin will recieve 4-6
    emails.
    

-- CONTACT --

Current maintainer:
* will_in_wi - http://drupal.org/user/116443
